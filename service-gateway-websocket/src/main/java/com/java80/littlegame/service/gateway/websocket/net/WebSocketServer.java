package com.java80.littlegame.service.gateway.websocket.net;

import com.java80.littlegame.service.gateway.websocket.GatewayConfig;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

/**
 * @author www.java80.com
 * @date 2018年3月11日 下午4:35:55
 */
public class WebSocketServer {
	EventLoopGroup bossGroup = new NioEventLoopGroup();
	EventLoopGroup workerGroup = new NioEventLoopGroup();

	public void start() throws InterruptedException {

		try {
			ServerBootstrap serverBootstrap = new ServerBootstrap();
			serverBootstrap.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class)
					.handler(new LoggingHandler(LogLevel.INFO)).childHandler(new WebSocketChannelInitializer());
			ChannelFuture channelFuture = serverBootstrap.bind(GatewayConfig.getServerport()).sync();
			System.out.println("websocket 网关启动成功，端口:" + GatewayConfig.getServerport());
			channelFuture.channel().closeFuture().sync();
		} finally {
			bossGroup.shutdownGracefully();
			workerGroup.shutdownGracefully();
		}
	}

	public void shutdown() {
		bossGroup.shutdownGracefully();
		workerGroup.shutdownGracefully();
	}

}
