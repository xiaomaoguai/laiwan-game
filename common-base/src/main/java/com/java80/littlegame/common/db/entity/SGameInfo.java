package com.java80.littlegame.common.db.entity;

import java.util.Date;

public class SGameInfo {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column s_game_info.id
     *
     * @mbggenerated Fri Apr 13 10:00:57 CST 2018
     */
    private Integer id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column s_game_info.gameName
     *
     * @mbggenerated Fri Apr 13 10:00:57 CST 2018
     */
    private String gameName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column s_game_info.createTime
     *
     * @mbggenerated Fri Apr 13 10:00:57 CST 2018
     */
    private Date createTime;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column s_game_info.id
     *
     * @return the value of s_game_info.id
     *
     * @mbggenerated Fri Apr 13 10:00:57 CST 2018
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column s_game_info.id
     *
     * @param id the value for s_game_info.id
     *
     * @mbggenerated Fri Apr 13 10:00:57 CST 2018
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column s_game_info.gameName
     *
     * @return the value of s_game_info.gameName
     *
     * @mbggenerated Fri Apr 13 10:00:57 CST 2018
     */
    public String getGameName() {
        return gameName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column s_game_info.gameName
     *
     * @param gameName the value for s_game_info.gameName
     *
     * @mbggenerated Fri Apr 13 10:00:57 CST 2018
     */
    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column s_game_info.createTime
     *
     * @return the value of s_game_info.createTime
     *
     * @mbggenerated Fri Apr 13 10:00:57 CST 2018
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column s_game_info.createTime
     *
     * @param createTime the value for s_game_info.createTime
     *
     * @mbggenerated Fri Apr 13 10:00:57 CST 2018
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}