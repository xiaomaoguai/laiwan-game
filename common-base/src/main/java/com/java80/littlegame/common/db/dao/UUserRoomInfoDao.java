package com.java80.littlegame.common.db.dao;

import java.sql.SQLException;
import java.util.List;

import com.java80.littlegame.common.db.dao.base.BaseUserDaoInterface;
import com.java80.littlegame.common.db.entity.UUserRoomInfo;

public interface UUserRoomInfoDao extends BaseUserDaoInterface<UUserRoomInfo> {
	public void delet(long userId, int roomId) throws SQLException;

	public void deleteByRoomId(int roomId) throws SQLException;

	public List<UUserRoomInfo> findByRoomId(int roomId) throws Exception;
}
