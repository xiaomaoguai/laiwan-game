package com.java80.littlegame.common.db.dao.dbImpl;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import com.java80.littlegame.common.db.dao.UUserRoomInfoDao;
import com.java80.littlegame.common.db.dao.base.BaseDao;
import com.java80.littlegame.common.db.entity.UUserRoomInfo;

public class UUserRoomInfoDaoDBImpl extends BaseDao implements UUserRoomInfoDao {

	@Override
	public void insert(UUserRoomInfo t) throws SQLException {
		String sql = "insert into u_user_room_info values(?,?,?,now())";
		super.insert(sql, Arrays.asList(t.getUserId(), t.getRoomId(), t.getGameId()));
	}

	@Override
	public void delete(long id) throws SQLException {
		// String sql="delete from u_user_room_info where "
		deleteByRoomId(Long.valueOf(id).intValue());
	}

	@Override
	public void update(UUserRoomInfo t) {

	}

	@Override
	public List<UUserRoomInfo> getAllByUserId(long userId) throws Exception {
		String sql = "select * from u_user_room_info where userId=?";
		return find(sql, Arrays.asList(userId), UUserRoomInfo.class);
	}

	@Override
	public void delet(long userId, int roomId) throws SQLException {
		String sql = "delete from u_user_room_info where userId=? and roomId=?";
		super.delete(sql, Arrays.asList(userId, roomId));
	}

	@Override
	public List<UUserRoomInfo> findByRoomId(int roomId) throws Exception {
		String sql = "select * from u_user_room_info where roomId=?";
		return find(sql, Arrays.asList(roomId), UUserRoomInfo.class);
	}

	@Override
	public UUserRoomInfo get(long id) {
		return null;
	}

	@Override
	public void deleteByRoomId(int roomId) throws SQLException {
		String sql = "delete from u_user_room_info where roomId=?";
		super.delete(sql, Arrays.asList(roomId));
	}

}
