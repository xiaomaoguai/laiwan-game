package com.java80.littlegame.service.game;

import com.hazelcast.config.Config;
import com.java80.littlegame.common.base.BaseHandler;
import com.java80.littlegame.common.base.BaseVerticle;
import com.java80.littlegame.common.base.Runner;
import com.java80.littlegame.common.base.ServiceStatus;
import com.java80.littlegame.common.base.SystemConsts;

import io.vertx.core.VertxOptions;
import io.vertx.spi.cluster.hazelcast.HazelcastClusterManager;

public class GameVerticle extends BaseVerticle {

	@Override
	public String queueName() {
		return GameConfig.getQueueName();
	}

	@Override
	public BaseHandler getHandler() {
		return new GameHandler();
	}

	@Override
	public ServiceStatus getServiceStatus() {
		ServiceStatus ss = new ServiceStatus();
		ss.setInstanceName(GameConfig.getInstanceName());
		ss.setServiceId(GameConfig.getServiceId());
		ss.setServiceQueueName(queueName());
		ss.setServiceType(SystemConsts.SERVICE_TYPE_GAME);
		return ss;
	}

	@Override
	public boolean needPublishServiceStatus() {
		return true;
	}

	public static void main(String[] args) {
		Runner.run(GameVerticle.class,
				new VertxOptions()
						.setClusterManager(new HazelcastClusterManager(new Config(GameConfig.getInstanceName())))
						.setClustered(true));
	}
}
