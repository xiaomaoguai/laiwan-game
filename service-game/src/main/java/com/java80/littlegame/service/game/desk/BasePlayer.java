package com.java80.littlegame.service.game.desk;

public class BasePlayer {
	private long playerId;
	private String nickName;

	public long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(long playerId) {
		this.playerId = playerId;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof BasePlayer) {
			return ((BasePlayer) obj).getPlayerId() == this.playerId;
		}
		return false;
	}

	@Override
	public String toString() {
		return "[playerId:" + this.playerId + "]";
	}

}
