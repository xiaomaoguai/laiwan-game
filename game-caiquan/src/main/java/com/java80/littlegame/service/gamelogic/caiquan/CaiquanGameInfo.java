package com.java80.littlegame.service.gamelogic.caiquan;

import java.util.ArrayList;
import java.util.List;

public class CaiquanGameInfo {
	private List<CaiquanPlayer> players = new ArrayList<>();// 局内玩家
	private List<CaiquanPlayer> actionPlayers = new ArrayList<>();// 当前可以动作的玩家比如哪些人可以出拳

	public List<CaiquanPlayer> getActionPlayers() {
		return actionPlayers;
	}

	public void setActionPlayers(List<CaiquanPlayer> actionPlayers) {
		this.actionPlayers = actionPlayers;
	}

	public List<CaiquanPlayer> getPlayers() {
		return players;
	}

	public void setPlayers(List<CaiquanPlayer> players) {
		this.players = players;
	}

	public CaiquanPlayer findPlayer(long id) {
		for (CaiquanPlayer p : players) {
			if (p.getPlayerId() == id) {
				return p;
			}
		}
		return null;
	}
}
